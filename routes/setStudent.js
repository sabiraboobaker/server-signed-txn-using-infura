var express = require("express");
const signTxn = require("./signTxn");
var router = express.Router();

router.post("/", function(req, res, next) {
  data = req.body;
  console.log(data);

  var publicAddress = data.publicAddr;
  var privateKey = data.privateKey;

  var methodCall = SMS.methods.setStudent(
    data.roll,
    data.name,
    data.age,
    JSON.parse(data.indian),
    data.gender
  );

  signTxn.sendTransaction(methodCall, publicAddress, privateKey, function(
    responce
  ) {
    if (responce == true) res.send("Student Registered !");
    else res.send("Transaction failed... Check Console for error...");
  });
});

module.exports = router;



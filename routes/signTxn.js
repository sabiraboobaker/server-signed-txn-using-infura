const Tx = require("ethereumjs-tx");

module.exports.sendTransaction = function (
  methodCall,
  addressFrom,
  privateKey,
  cb
) {
  const encodedABI = methodCall.encodeABI();
  const privateKeyHex = Buffer.from(privateKey, "hex");

  web3.eth.getGasPrice()
  .then(gPrize =>{
    web3.eth.getTransactionCount(addressFrom).then(txCount => {
      web3.eth.getBlock("latest").then(block => {
        var txData = {
          nonce: web3.utils.toHex(txCount),
          gasLimit: web3.utils.toHex(block.gasLimit),
          gasPrice: web3.utils.toHex(gPrize * 2 ),
          from: addressFrom,
          to: contractAddress,
          data: encodedABI
        };

        const transaction = new Tx(txData);
        transaction.sign(privateKeyHex);
    
        const serializedTx = transaction.serialize().toString("hex");

        web3.eth
          .sendSignedTransaction("0x" + serializedTx)
          .on("transactionHash", function (hash) {
            console.log("transactionHash: " + hash);
          })
          .on("receipt", function (receipt) {
            console.log("receipt: ");
            console.log(receipt);
            cb(true);
          })
          .on("error", function (error) {
            console.log("error: " + error);
            cb(false);
          })
          .catch(function (error) {
            console.log("*====Web3 Exception Handled====*\n");
            console.log(error);
            console.log("\n*==============================*");
          });
      })
    });
  });
};
